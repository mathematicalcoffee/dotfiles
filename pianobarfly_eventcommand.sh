#!/bin/bash
# Modified from https://github.com/dpfried/scripts/blob/master/pianobar-notify.sh

while read L; do
    k="`echo "$L" | cut -d '=' -f 1`"
    v="`echo "$L" | cut -d '=' -f 2`"
    export "$k=$v"
done < <(grep -e '^\(title\|artist\|album\|songStationName\|stationName\|pRet\|pRetStr\|wRet\|wRetStr\|songDuration\|songPlayed\|rating\|coverArt\)=' /dev/stdin) # don't overwrite $1...

if [ "$1" == "songstart" ]; then
    if [ "$stationName" == "QuickMix" ]; then
        notify-send "$title" "$artist ($album)\n[$songStationName @ $stationName]"
    else
        notify-send "$title" "$artist ($album)\n[$stationName]"
    fi
fi
