#!/bin/sh
# YOU NEED TO BE ROOT; this should only need to be done once.
# should only need to be one-time. Then all I have to do is remember to switch
#  keyboard layouts back for shakes (OH, but evdev?)
# http://askubuntu.com/questions/482678/how-to-add-a-new-keyboard-layout-custom-keyboard-layout-definition

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# 1. add symbols file and 10-keyboard.conf
ln -s /home/amy/dotfiles/us_amy /usr/share/X11/xkb/symbols/
# ugha pparently there's a "keyboard Layout Editor" gui for this!
ln -s /home/amy/dotfiles/99-keyboard.conf /usr/share/X11/xorg.conf.d/

# 2. add to evdev.xml
text='   <layout>
      <configItem>
        <name>us_amy</name>

        <shortDescription>amy</shortDescription>
        <description>US-en with lvl3 programming keys (Amy)</description>
        <languageList>
                 <iso639Id>eng</iso639Id>
        </languageList>
      </configItem>
      <variantList>
        <variant>
          <configItem>
            <name>nokp</name>
            <shortDescription>amy_nokp</shortDescription>
            <description>US-en, lv3 programming, no keypad (Amy)</description>
            <languageList>
              <iso639Id>eng</iso639Id>
            </languageList>
          </configItem>
        </variant>
      </variantList>
    </layout>'
evdev=/usr/share/X11/xkb/rules/evdev.xml
evdevbak=${evdev}.bak
if [ -f $evdevbak ]; then
    cp $evdevbak $evdev
else
    cp $evdev $evdevbak
fi
ed - $evdev << END
/<layoutList>/a
$text
.
wq
END

# 3. refresh
dpkg-reconfigure xkb-data
