#!/usr/bin/env python

import glib
import dbus
import subprocess
import re
from dbus.mainloop.glib import DBusGMainLoop

keywords_regex = re.compile("Herston")
subject_regex = re.compile(r'BrisbanePogoMap \((?P<chatroom>#.+?)\)')
body_regex = re.compile(r'\[(?P<suburb>[^\]]+)\] \*\*(?P<pokemon>.+?)\*\* \((?P<IV>[^ ]+)\) - \(CP: (?P<CP>[-0-9]+)\) - \(Level: (?P<level>[-0-9]+)\).+\*\*L30\+ IV\*\*: (?P<stats>[-0-9 ]+)  \(', re.DOTALL)

def msg_cb(bus, msg):
    # OK. You can't seem to save the args and then access them. You seem to have to
    #   call msg.get_args_list() each time. Then you're fine.
    tmp = msg.get_args_list(utf8_strings=True)
    if len(tmp) > 3 and msg.get_args_list()[0] == 'Discord':
        subprocess.Popen(["pkill", "notify-osd"]) # kill notification
        body = msg.get_args_list(utf8_strings=True)[4]

        title = msg.get_args_list(utf8_strings=True)[3]
        chatroom = subject_regex.search(title).group('chatroom')
        m = body_regex.search(body)
        #print ['%s: %s' % (x, m.group(x)) for x in ['suburb', 'pokemon', 'CP', 'IV', 'stats']]
        title = '[%s] %s (%s)' % (m.group('suburb'), m.group('pokemon'), chatroom)
        body = '%s (%s) CP%s L%s' % (m.group('stats'), m.group('IV'), m.group('CP'), m.group('level'))
        print title
        print body
        print "-----"

        if keywords_regex and keywords_regex.match(body): # if it matches the suburb of interest, notify
            subprocess.Popen(['notify-send', title, body])
            subprocess.Popen(['aplay', './notif.wav'])

if __name__ == '__main__':

    DBusGMainLoop(set_as_default=True)
    bus = dbus.SessionBus()

    # MethodCall messages
    bus.add_match_string("eavesdrop=true, type='method_call', interface='org.freedesktop.Notifications', member='Notify'")
    bus.add_message_filter(msg_cb)

    mainloop = glib.MainLoop ()
    mainloop.run ()
