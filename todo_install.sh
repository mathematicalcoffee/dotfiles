#!/bin/bash

# bash_aliases and bashrc are already modified
chmod +x todo.txt/todo.sh
git submodule init
git submodule update --init -- todo.txt/pomodori-todo.txt

pushd todo.txt/plugins
for f in *; do
    chmod +x "$f"
done
popd ../../

pushd $HOME
git clone git@bitbucket.org:mathematicalcoffee/todos.git todos
ln -s todos/todo.txt .
ln -s dotfiles/todo.txt .todo
ln -s ../.todo/todo.sh bin/todo.sh
# for pomodoro plugin
gem install rainbow terminal-notifier
popd
echo "Done. Todo list is in ~/todo.txt."
