#' Returns a vector where all but the first element are replaced by NA
#'
#' A bit stupid. For use when I want to make a results table where there might be
#' say 3 rows for one subject and I only want to display the subject once
#' @family pandoc helpers
#' @export
stoopid.blank <- function(x, n=length(x)) {
    c(x[1], rep(NA, n - 1))
}

#' Sprintf numbers and bold the largest
#'
#' Really supposed to be used in a data.table with a `by` term.
#' @family pandoc helpers
#' @export
bold.largest <- function (x, fmt='%.2f', symbol='**') {
    surr <- ifelse(x == max(x), symbol, '')
    sprintf(paste0('%s', fmt, '%s'), surr, x, surr)
}

#' A cheating way to format image grids. Outputs a table of ![]() with subcaptions.
#'
#' Should really use knitr's fig.subcap or something but it doesn't seem to work
#' (it puts all the subfigures on one line).
#' Also the captions only show if the image is a plot output; if it's just a
#' ![caption](image.png) the caption doesn't show. So I've put it in explicitly.
#' @family pandoc helpers
#' @export
image.table <- function(paths, nrow=NULL, ncol=NULL, caption='', subcaptions='', byrow=T, fill=T, number=F, rownames=NULL, colnames=NULL,...) {
    if (is.null(nrow) && is.null(ncol))
        stop("nrow and/or ncol must be provided")
    if (!is.null(nrow) && !is.null(ncol)) {
    }
    n <- length(paths)
    subcaptions <- rep(subcaptions, length.out=n)
    empty <- which(paths == "" | is.na(paths))
    if (number)
        subcaptions <- paste0('(', letters[1:n], ') ', subcaptions)
    contents <- unname(mapply(pandoc.image.return, paths, subcaptions))
    contents[subcaptions != ''] <- paste0(contents[subcaptions != ''], '  \n', subcaptions[subcaptions != ''])

    if (is.null(nrow))
        mm <- matrix(contents, ncol=ncol, byrow=byrow)
    else if (is.null(ncol))
        mm <- matrix(contents, nrow=nrow, byrow=byrow)
    else
        mm <- matrix(contents, nrow=nrow, ncol=ncol, byrow=byrow)
    if (length(mm) > n && fill)
        mm[(n+1):length(mm)] <- ""
    mm[empty] <- ""
    if (!is.null(rownames)) base::rownames(mm) <- rownames
    if (!is.null(colnames)) base::colnames(mm) <- colnames

    pandoc.table(mm, keep.line.breaks=T, caption=caption, ...)
    return(invisible(mm))
}

#' Outputs LaTeX code for a figure environment with caption and label and placement
#'
#' The RMD -> MD conversion drops `fig.pos` and labels when you run knitr so I
#' need to output LaTeX explicitly.
#'
#' Use Hmisc or xtab if you want tables.
#'
#' If you want to do subfigures (using subfloat) then provide multiple filenames;
#'  the first element of the caption is the overall and the rest are individual.
#' @family pandoc helpers
#' @export
latex.figure <- function (fname, caption, label, placement='!t', floating=F, width='\\columnwidth', subfloat=length(fname) > 1, linebreaks.after=NULL) {
    if (subfloat && length(caption) == length(fname))
        caption <- c('', caption)
    else if (length(caption) > 1 && length(caption) != length(fname) && length(caption) != length(fname) + 1)
        stop("Length of filenames doesn't match length of captions (+1 if subfloat)")
    cat(sprintf('\\begin{figure%s}[%s]%%\n\\centering%%\n',
                ifelse(floating, '*', ''), placement))
    figs <- sprintf('\\includegraphics[width=%s]{%s}', width, fname)
    if (subfloat)
        figs <- sprintf('\\subfloat[%s]{%s}', caption[2:(length(fname) + 1)], figs)
    if (!is.null(linebreaks.after))
        figs[linebreaks.after] <- paste0(figs[linebreaks.after], '\\\\')
    figs <- paste0(figs, '%')
    cat(figs, sep='\n')
    # TODO should omit \caption{} if not provided for subfloat (also for normal pics)
    cat(sprintf('\\caption{%s}%%\n\\label{%s}%%\n\\end{figure%s}\n',
        caption[1], label, ifelse(floating, '*', '')))
}

rotx90 <- function () { ggplot2::theme(axis.text.x = ggplot2::element_text(angle = 90, hjust = 1)) }
