# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

############################# End Ubuntu boilerplate ###########################

## R
export EDITOR=vim

## misc. path edits
# Checks if the argument exists and is a directory and prepends it to PATH.
# http://superuser.com/questions/39751/add-directory-to-path-if-its-not-already-there
pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="$1"${PATH:+":$PATH"}
    fi
}
pathadd $HOME/bin
pathadd $HOME/.local/bin
pathadd /opt/pycharm-community-2018.1.3/bin
# texlive
pathadd /usr/local/texlive/2018/bin/x86_64-linux

# line wrap git log -n 1
export LESS=-FRX

# have a random cow say a fortune, if the user has both fortune & cowsay installed.
# (TODO: maintain the nhqdb as fortunes)
#type fortune &> /dev/null && type cowsay &> /dev/null && (fortune | toilet --gay -f term | cowsay -n -f $(ls /usr/share/cowsay/cows/ | shuf -n1))
type fortune &> /dev/null && type cowsay &> /dev/null && ((type lolcat &> /dev/null && (fortune | cowsay -n -f $(ls /usr/share/cowsay/cows/ | shuf -n1) | lolcat)) || \
    (type toilet &> /dev/null && (fortune | toilet --gay -f term | cowsay -n -f $(ls /usr/share/cowsay/cows/ | shuf -n1))) || \
    (fortune | cowsay -n -f $(ls /usr/share/cowsay/cows/ | shuf -n1)))
# fortune | toilet --gay -f term | cowsay -n

# Polymathian Postgres
export PGHOST="localhost"
export PGUSER="webapp"
export PGPASSWORD="webapp"

# Polymathian ccache
pathadd /usr/lib/ccache

# Polymathian gurobi
export GUROBI_HOME="/opt/gurobi810/linux64"
export PATH="${PATH}:${GUROBI_HOME}/bin"
export GUROBI_LIB="gurobi80"
export GRB_LICENSE_FILE="/opt/gurobi810/gurobi.lic"
export CPLEX_HOME="/opt/cplex128"
export CC=`which gcc-8`
export CXX=`which g++-8`

#export COIN_OSI_HOME="/opt/coin-Clp"

# for crontab to change my wallpaper with randomWallpaperChanger.pl
if [ "$DISPLAY" != "" ] ; then
    xhost local:$USER > /dev/null;
fi


# rfmt: R source code formatter
alias rfmt="python '/home/amy/R/x86_64-pc-linux-gnu-library/3.4/rfmt/python/rfmt.py' --indent 4 --space_arg_eq False"

# fonts
source $HOME/.fonts/devicons-regular.sh  $HOME/.fonts/fontawesome-regular.sh  $HOME/.fonts/octicons-regular.sh  $HOME/.fonts/pomicons-regular.sh

