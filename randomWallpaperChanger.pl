#!/usr/bin/perl
#
# randomWallpaperChanger_amy.pl
#
# MODIFIED by AC 25-Jul-2010
#  Adjusted behaviour for dual monitors.
#  What it does is resize each image (individually) onto a canvas that will
#   fit on its monitor. Then these images are appended together with top/left
#   gravity. Imagemagick will fill the rest of the space with black background.
#  I've found this works on nvidia twinview and in effect fits each individual
#   image to each individual resolution, as opposed to the previous version
#   which resized the image to the -smaller- monitor.
#
# Also, set the Desktop image mode to 'span' as opposed to 'tile' or 'fill' or
#  'zoom' (that's what it's called on Ubuntu anyhow).
#
# Copyright (C) 2009 Nathan Shafer
#
# This program is free software; you can redistribute it and/or              *
# modify it under the terms of the GNU General Public License                *
# as published by the Free Software Foundation; either version 2             *
# of the License, or (at your option) any later version.                     *
#                                                                            *
# This program is distributed in the hope that it will be useful,            *
# but WITHOUT ANY WARRANTY; without even the implied warranty of             *
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
# GNU General Public License for more details.                               *
#                                                                            *
# You should have received a copy of the GNU General Public License          *
# along with this program; if not, write to the Free Software                *
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.*
#
# ------------------------------------------------------------------------------
# Description
# ------------------------------------------------------------------------------
# This script will randomly change Gnome's wallpaper.  It supports a single
# monitor or multiple monitors by stitching together different wallpapers for
# each screen.  Simply configure a few options below then run this script.
#
# Here are a few notes for multiple monitors:
# - The monitors can be stacked vertically or set up horizontally side by side,
#   but no other physical setups are supported.  The reason is that we're
#   creating one big image to make this work, so the image has to be a rectangle.
#   So for example, with 2 monitors of 1600x1200, the script creates an image
#   that is 3200x1200 in size for a horizontal layout.  A vertical layout would
#   create 1600x2400.
# - If you have mismatched resolution sizes, we will create the image based on
#   the smallest resolution of the monitors.  So for example, a 1600x1200 next
#   to a 1024x768 will result in an image of (horizontally) 2048x768.  I don't
#   know what will happen if it creates the image at the max resolution of the
#   monitors... I don't have mismatched resolutions.  If someone wants to test
#   the two options and tell me which is better, I would appreciate it.
#
# ------------------------------------------------------------------------------
# Requirements
# ------------------------------------------------------------------------------
# This script was developed and tested on linux.  Other OSes will probably work
#
# It requires Perl of course.
#
# It also only works with Gnome
#
# This script requires you have PerlMagick installed.  This can be installed
# from CPAN, but on debian based linux distros, you can install it with
#   sudo apt-get install perlmagick
# and on redhat based linux distros, this should work:
#   sudo yum install ImageMagick-perl
#
# ------------------------------------------------------------------------------
# Installation
# ------------------------------------------------------------------------------
# Simply put the script somewhere and execute it.
#
# This script works really well as a startup item to change your wallpaper
# every time you log in, or as a cron to change it on a set schedule.  For
# example, edit your crontab with "crontab -e" and put this in it:
#
#   */30 * * * * /path/to/random_wallpaper.pl
#
# That will change it every 30 minutes.  Change the 30 to whatever interval you want
#

use strict;
use Image::Magick;
use Gtk2 -init;
use vars qw($x);


# NOTE: do 'export DISPLAY=:0.0 && $HOME/randomWallpaperChanger in your crontab, and
# xhost local:your_user_name > /dev/null in your .bashrc
#print "ENV{DISPLAY}||".$ENV{DISPLAY}."||";
# This used to work but doesn't on fedora?!
#if ($ENV{DISPLAY} eq "") {
#$ENV{DISPLAY} = ":0.0";
#};


################################################################################
# OPTIONS
################################################################################

# Specify the orientation of your monitors as either "horizontal" or "vertical"
# i.e. monitors are physically horizontally next to each other, or vertically panelled.
# For now can't combine.
my $orientation = "horizontal";

# tile, zoom, center, scale (default), fill
# tile: repeat to fill screen entirely
# zoom: zooms in until image fills screen (preserve ratio) (windows: 'fill')
# center: center image without resizing
# scale: enlarge until image meets at least one of the edges (windows: 'fit')
# stretch: stretches to monitor's ratio (does not preserve image ratio)
my $style = 'zoom';
# tile: convert IMG -extent 1280x1024 -tile IMG -draw 'rectangle 0,0,WIDTH-1,HEIGHT-1' out.jpg
# zoom: convert IMG -gravity center -resize WIDTHxHEIGHT^ -extent WIDTHxHEIGHT
# centre: convert IMG -extent WIDTHxHEIGHT -gravity center
# scale: convert IMG -resize WIDTHxHEIGHT -gravity center -extent WIDTHxHEIGHT
# stretch: convert IMG -resize WIDTHxHEIGHT\!

# Add the full path to each directory you want to pull images from randomly
my @wallpaper_dirs = (
  "$ENV{HOME}/wallpapers"#,
#  "$ENV{HOME}/wallpapers/ISS_Andre_Kuipers"
);

# Specify the file to output the final image to
my $output_dir = "/tmp";
my $output_file = $output_dir . "/change_wallpaper_output.jpg";

# Turn debugging on or off with either 1 or 0
my $debug = 0;

################################################################################
# END CONFIG - DO NOT EDIT ANYTHING BEYOND THIS UNLESS YOU KNOW WHAT YOU'RE DOING
################################################################################
my $screen = Gtk2::Gdk::Screen->get_default();
my @screens = ();

# Automagically detect screen layout!
for (my $i=0; $i<$screen->get_n_monitors(); $i++)
{
    my $rect = $screen->get_monitor_geometry($i);
#    push(@screens, [$rect->width,$rect->height]);
    push(@screens, $rect);
}

# sort screens by position left to right top to bottom
sub byPosn{
    if ($a->x == $b->x ) { return $a->y <=> $b->y; }
    else { return $a->x <=> $b->x; }
}


@screens = sort byPosn @screens;
# for my $scrn( @screens ) { print $scrn->x, ", ", $scrn->y, "\n"; }

# Load the list of possible wallpapers into memory
my @wallpapers;
foreach my $dir (@wallpaper_dirs) {
  opendir(DIR, $dir) or die("Could not open directory $dir: $!\n");
  while(my $file = readdir DIR) {
    next if $file =~ /^\./;
    if($file =~ /.jpg|.jpeg|.png|.bmp$/i) {
      push(@wallpapers, "$dir/$file");
    } else {
      print "invalid file in $dir: $file\n" if $debug;
    }
  }
}

# TODO: ORIENTATION

# Fetch a random wallpaper for each screen, resize appropriately.
# while you're at it, assemble the output command!
my $dummy=0;
my $finalCommand = "convert";

foreach my $screen (@screens) {
    my $cmd = "convert " . $wallpapers[int(rand(@wallpapers))]." -background black ";
    my $sizeString = $screen->width."x".$screen->height;

    if ( $style eq "tile" ) {
        # tile: convert IMG -extent 1280x1024 -tile IMG -draw 'rectangle 0,0,WIDTH,HEIGHT' out.jpg
        $cmd = "$cmd -extent $sizeString -tile ".$wallpapers[int(rand(@wallpapers))]." -draw 'rectangle 0, 0, ".$screen->width . "," . $screen->height . "'";
    } elsif ( $style eq "zoom" ) {
        # zoom: convert IMG -gravity center -resize WIDTHxHEIGHT^ -extent WIDTHxHEIGHT
        $cmd = "$cmd -gravity center -resize $sizeString^ -extent $sizeString";
    } elsif ( $style =~ /^cent(er|re)$/ ) {
        # centre: convert IMG -gravity center -extent WIDTHxHEIGHT 
        $cmd = "$cmd -gravity center -extent $sizeString";
    } elsif ( $style eq "stretch" ) {
        # stretch: convert IMG -resize WIDTHxHEIGHT\!
        $cmd = "$cmd -resize $sizeString\\!";
    } else { # scale
        # scale: convert IMG -resize WIDTHxHEIGHT -gravity center -extent WIDTHxHEIGHT
        $cmd = "$cmd -resize ${sizeString} -gravity center -extent ${sizeString}";
    }
    $cmd = "$cmd ${output_dir}/randomWallpaperChangerOutput_${dummy}.png";
    print "command: $cmd\n" if $debug;
    system($cmd);
    #system("convert " . $wallpapers[int(rand(@wallpapers))]." -background black -resize ".$screen->width."x".$screen->height." -gravity center -extent ".$screen->width."x".$screen->height." ${output_dir}/randomWallpaperChangerOutput_${dummy}.png");
	$finalCommand = "$finalCommand ${output_dir}/randomWallpaperChangerOutput_${dummy}.png";
	$dummy++;
}
$finalCommand = "$finalCommand -background black -gravity north +append $output_file";
print "final command: $finalCommand\n" if $debug;
system($finalCommand);
# Now tell Gnome to change the wallpaper
# ** For Gnome 3 (gnome-shell)
system("gsettings set org.gnome.desktop.background picture-uri 'file://$output_file'");
# For Gnome 2:
# system("/usr/bin/gconftool-2 --type string --set /desktop/gnome/background/picture_filename $output_file");


