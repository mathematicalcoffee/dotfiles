" My changes
" tab == 4 spaces
set tabstop    =4
set shiftwidth =4
set sw         =4
set expandtab

set history=50      " keep 50 lines of command line history (default 20)
set ruler           " show the cursor position all the time
set showcmd         " display incomplete commands
set incsearch       " do incremental searching

set nocompatible " vi-compatible
syntax enable
filetype plugin on
filetype plugin indent on
filetype indent on

" =========== COLOUR =========== "
" pathogen (any files in the bundle/ folder get installed automagically)
runtime bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()

" select colour scheme (transparent background though)
colorscheme desert256
hi Normal ctermbg=NONE
hi NonText ctermbg=NONE
set cc=80
hi ColorColumn ctermbg=DarkGray " black bg for cc so it doesn't stand out so much. or try 8 (grey)


" slow vim on Ruby
" http://stackoverflow.com/questions/16902317/vim-slow-with-ruby-syntax-highlighting
" TODO later: for ruby files only?
set re=1

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
   set mouse=vi "a
endif

" Hmm, should probably only do this for code files: python, R, ...
"autocmd FileType cpp,c,cxx,h,hpp,python,sh,r,perl
"autocmd FileType markdown,pandoc,html,ruby,yaml setlocal tabstop=2 shiftwidth=2


" <space> to fold/unfold
nnoremap <space> za

" try synchronise vim's default copy/paste register with the system one
set clipboard^=unnamed

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " Python: see ftplugin/python.vim
  " R: see ftplugin/r.vim

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  " Also don't do it when the mark is in the first line, that is the default
  " position when opening a file.
  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent        " always set autoindenting on

endif " has("autocmd")

" pressing F3 will toggle between paste mode
set pastetoggle=<F3>

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
 if !exists(":DiffOrig")
   command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
 endif

" ======== LEADER ======= "
" Used in the showmarks and R plugins. Although ',' is nice it's very common
" and in insert mode slows down a little to see if you meant to type anything 
" else after it :( 
" I changed it to ';' which although I use it often always with a spac after
" it.
let maplocalleader=";"
let mapleader=";"

" =========== FOLDING =========== "
" za toggles folding, zo opens a fold, zc closes a fold.
"set foldmethod=indent  " fold based on indent
set foldmethod=syntax   " fold based on syntax
set foldenable          " folding enabled by default
set foldlevelstart=99   " folds open by default.

" =========== ALIASES =========== "
" do ,E to insert new line before current, ,e for after.
"nmap ,E O<Esc>
"nmap ,e o<Esc>

" -- Backspace, Enter, tab all work in normal mode: (delete already does)
" press Enter to insert new line before cursor
nmap <Enter> i<Enter><Esc>
"nmap <Backspace> "zdh
nmap <Tab> i<Tab><Esc>h 

"restore backspace functionality (wouldn't let me backspace over insertion
"point)
set bs=2 

" do ,. to go to end of sentence and make a new line
nmap ,. )i<Enter><Esc>
" split to next line at comma (removing trailing spaces)
nmap ,, f,li <Esc>dwi<Enter><Esc>

" =========== PLUGINS =========== "
"@ " if you place them $HOME/.vim then you don't have to load them yourself...
"@ "source $HOME/.vim/plugins/ScrollColor.vim	" allows to scroll thru colour schemes with :SCROLL
"  CSApprox --> makes various colour schemes for GVim work transparently for
"  console
"  ColourSamplerPack - 140 colour schemes
"@ "showmarks.vim          " shows your marks (ma, g'a etc)
"@ ",mt toggles it on and off.
"@ ",ma hides all marks in current buffer
"@ ",mm places next available mark.
"@ let showmarks_include="abcdefghijklmnopqrstuvwxyz" " only show marks a--z being the user-set ones.
"@ 
" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! %!sudo tee > /dev/null %

" -- copy paste --
" http://vim.wikia.com/wiki/In_line_copy_and_paste_to_system_clipboard
" Copy-paste to system clip on ubuntu (running Vim in gnome-terminal)
" if xclip < .11: vmap <C-c> y:call system("xclip -i -selection clipboard", getreg("\""))<CR>:call system("xclip -i", getreg("\""))<CR>
vmap <C-c> y: call system("xclip -i -selection clipboard", getreg("\""))<CR>
nmap <C-v> :call setreg("\"",system("xclip -o -selection clipboard"))<CR>p
" lets you Cv in insert mode and keep typing
imap <C-v> <Esc><C-v>a

" rfmt: R source code formatter --> actually like styler/tidyverse a bit
" better
let rfmt_executable = '/home/amy/R/x86_64-pc-linux-gnu-library/3.4/rfmt/python/rfmt.py'
map <C-I> :pyf /home/amy/R/x86_64-pc-linux-gnu-library/3.4/rfmt/python/rfmt_vim.py<cr>
imap <C-I> <c-o>:pyf /home/amy/R/x86_64-pc-linux-gnu-library/3.4/rfmt/python/rfmt_vim.py<cr>


"Nvim-R
let R_assign = 0

" lintr + syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

" Use :SyntasticCheck (passive mode default)
"let g:syntastic_enable_r_lintr_checker = 1
let g:syntastic_r_checkers = ['lintr']
"let g:syntastic_r_linters = "with_defaults(object_name_linter(style='dotted.case'))"
let g:syntastic_mode_map = {
    \ "mode": "passive",
    \ "active_filetypes": [],
    \ "passive_filetypes": [] }
