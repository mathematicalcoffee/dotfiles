#!/bin/sh

if [ "$(cat ~/.mouse-state)" = "0" ]; then
    echo 1 > ~/.mouse-state;
    pids="$( pgrep -o -f 'bash .*clicker.sh' )"; # take the oldest (we are assuming `bash X` is used to launch from keyboard shortcut)
    #echo $pids;
    kill $pids
else
    echo 0 > ~/.mouse-state;
    while true; do
        xdotool click 1;
        sleep 0.05
    done
fi
