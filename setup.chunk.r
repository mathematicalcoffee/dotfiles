muffle <- function (...) {
    # warning from oro.nifti
    capture.output({x <- suppressWarnings(suppressPackageStartupMessages(suppressMessages(...)))})
    invisible(x)
}

muffle({
library(ggplot2)
library(rphd)
library(data.table)
library(png)
library(pander)
library(knitr)
library(xtable)
library(oro.nifti)
library(english)

library(lmerTest)
library(emmeans)
loadNamespace('pbkrtest')
})

nocows <- F
if (exists('params'))
    nocows <- ifelse(is.null(params$nocows), F, params$nocows)

# --- knitr ---
FIG.PATH='../figure/'
opts_knit$set(fig.path=FIG.PATH, eval.after=c('fig.cap', 'fig.subcap'))
if (!file.exists(FIG.PATH)) dir.create(FIG.PATH, recursive=T)
# out.height=\textheight or else small images not enlarged (knitr #1477)
opts_chunk$set(echo=F, comment=NA, results='asis', fig.align='center', out.width='50%', out.height='\\textheight')
# if you put figref=TRUE on a chunk that makes figures it'll put the
#  {#fig:chunk label} in ... bit of a hack.
# Should only do it for HTML - not for latex
# @TODO if using include_graphics it doesn't work because that outputs a <div>...
# <div class="figure" style="text-align: center">
# <img src="../figure/ch1/pn5_1.png" alt="**TODO**: diagram indicating CSF, GM, WM" width="40%" />
# <p class="caption">**TODO**: diagram indicating CSF, GM, WM</p>
# </div> {#fig:fig.intro.tissues}
# @UPTO ahh it's because I'm compiling to MD first. Why is this again?
# When I make the thesis I compile each chapter to MD, concatenate. This is to stop
#  chunks interfering with each other (I think).
# When I make each chapter, I want to compile directly from RMD to {HTML|PDF}. Hmmm...
knit_hooks$set(figref = function (before, options, envir) {
    # message(rmarkdown::all_output_formats(knitr::current_input())) # <-- has both html_document and pdf_document
    # print(opts_knit$get('rmarkdown.pandoc.to')) # 'latex' or 'html'.
    oformat <- opts_knit$get('rmarkdown.pandoc.to')
    if ((is.null(oformat) || oformat == 'html') && !before) {
        return(sprintf(' {#fig:%s}\n', options$label))
    }
})

# --- xtable --- #
options(xtable.caption.placement='top',
        xtable.table.placement='!t',
        xtable.include.rownames=F,
        xtable.comment=F)

source(system.file('pandoc-helpers.r', package='rphd')) # stoopid.blank image.table and bold.largest

# important annotation functions.
if (muffle(require(cowsay))) {
    moo <- function (...) cowsay(...)
    urgentcow <- function (x, ...) moo(paste0("!!URGENT: ", x, '!!'), ..., cow='satanic')
    todolistcow <- function (x, ...) moo(c("TODO:", paste("- ", x)), ..., style='wired')
    todocow <- function (x, ...) moo(paste("TODO:", x), ..., style='wired')
    iancow <- function (x, ...) moo(paste("Ian:", x), ..., cow='unipony-smaller')
    jurgencow <- function (x, ...) moo(paste("Jurgen:", x), ..., cow='moose')
    notecow <- function (x, ...) moo(paste("Note:", x), ...)
    quoteverb <- function (x) pandoc.p(gsub('\n|^', '\n>     ', x))

    # left out some I don't particularly like.
    # I like also sheep, suse, small but they broken.
    smallcows <- c('duck', # 7 lines with a one-line quote
                   'bunny', 'default', 'mutilated', 'three-eyes', 'www',  # 8
                   'apt', 'hellokitty', 'pony-smaller', # 9
                   'kitty', 'moose', # 10
                   'unipony-smaller', 'vader', # 11 # sheep broken (conv to R)
                   'cower', 'moofasa', 'skeleton', 'tux') # 12
    smallcowsay <- function(...) cowsay(..., cow=sample(smallcows, 1))

    # say a stupid fortune / nethack quote?
    if (!is.null(cf <- current_input(dir=T))) {
        library(tools)
        # try to get a unique quote per file md5sum
        # {could try instead per version but then I have to remember to increment it}
        sd <- strtoi(substring(md5sum(cf), 1, 7), 16L)
        #quoteverb(cf)
        #quoteverb(sd)
        set.seed(sd)
    }
    if (muffle(require(fortunes)) && !nocows) {
        f <- fortune()
        quoteverb(suppressMessages(smallcowsay(f, wrap=-1)))
    }
}

# hr <- F
# say the notes (if any)
# if (!is.null(rmarkdown::metadata$note)) {
#     hr <- T
#     cat("#### Notes {-}\n")
#     pandoc.p(rmarkdown::metadata$note)
# }
# # say the todos
# if (!is.null(rmarkdown::metadata$todo)) {
#     hr <- T
#     cat("#### Todo {-}\n")
#     pandoc.list(rmarkdown::metadata$todo)
# }
# if (hr)
#     pandoc.horizontal.rule()

# --- rphd --- #
dat.dir <- '~/phd/data'
get.MRI2 <- function(.subject, .type, dat, dat.dir='~/phd/data', ...)
    get.MRI(subset(dat, subject == .subject & type == .type), dat.dir, ...)

# --- pandoc --- #
panderOptions('table.split.table', Inf)
panderOptions('table.split.cells', Inf)
panderOptions('p.wrap', '')

# --- editing? --- #

tbl <- function (x, caption, ...) {
    if (!isTRUE(getOption('knitr.in.progress'))) {
        print(x)
        if (!missing(caption)) cat('\nTable: ', caption=caption)
    } else {
        if (missing(caption)) caption <- NULL
        pandoc.table(x, caption=caption,...)
    }
}

#  from inst/pandoc-helpers.r
# Returns a vector where all but the first element are replaced by NA
# A bit stupid. For use when I want to make a results table where there might be
# say 3 rows for one subject and I only want to display the subject once
stoopid.blank <- function(x, n=length(x)) {
    c(x[1], rep(NA, n - 1))
}

pct <- function (x, dp=0) {
    sprintf(paste0('%.', dp, 'f%%'), x * 100)
}
bold <- function (x, symbol='**', latex=FALSE) {
    ifelse(latex, sprintf('\\textbf{%s}', x), sprintf('%s%s%s', symbol, x, symbol))
}
# Sprintf numbers and bold the largest
# res[, accuracy=bold.largest(mean(accuracy)), by=.(potential)]
bold.largest <- function (x, fmt='%.2f', symbol='**', latex=FALSE) {
    ss = ifelse(latex, '\\textbf{', symbol)
    se = ifelse(latex, '}', symbol)
    ss = ifelse(x == max(x), ss, '')
    se = ifelse(x == max(x), se, '')
    sprintf(paste0('%s', fmt, '%s'), ss, x, se)
}
pval <- function (x, fmt='%.2f', cutoff=0.01, sig.cutoff=0.05, sig.mark='*') {
    sfx <- ifelse(x < sig.cutoff, sig.mark, '')
    paste0(ifelse(x < cutoff,
           sprintf(paste0('<', fmt), cutoff),
           sprintf(fmt, x)), sfx)
}

# escape underscore - if you put it in a fig caption you get a \textbackslash{}begin\{figure\} rather than \begin{figure}
esc <- function (x) gsub('_', '\\\\_', x)
# formats all numeric columns in a table using `bold.largest`
# main reason is that you want to sort it then format it, saves some typing
# format.results.table(
#  res[, list(accuracy=mean(accuracy)), by=.(potential)][-(accuracy)]
# )
# vs
# res[, list(accuracy=mean(accuracy)), by=.(potential)][-(accuracy)][,
#       list(potential, accuracy=bold.largest(accuracy))]
format.results.table <- function (x, ..., cols=-1) {
    if (missing(cols)) cols <- names(Filter(isTRUE, lapply(x, is.numeric)))
    x[, c(cols):=lapply(x[, cols, with=F], bold.largest,...)]
    x
}
# ???
tbl.ANOVA <- function (x, fmtDF='%.1f', fmtSS='%.3f', pval.args=list(fmt='%.3f', cutoff=0.001), justify='rcccccc', ..., return=FALSE) {
    rownames(x) <- gsub('`', '', rownames(x))
    colnames(x)[colnames(x) == 'F.value'] <- 'F'
    x[, 'Pr(>F)'] <- do.call(pval, c(x[, 'Pr(>F)'], pval.args))
    x[, 'F'] <- sprintf(fmtSS, x[, 'F'])
    x[, 'DenDF'] <- sprintf(fmtDF, x[, 'DenDF'])
    x[, 'Sum Sq'] <- sprintf(fmtSS, x[, 'Sum Sq'])
    x[, 'Mean Sq'] <- sprintf(fmtSS, x[, 'Mean Sq'])
    if (return)
        as.data.frame(x)
    else
        tbl(as.data.frame(x),
            emphasize.rownames=F,
            justify=justify,
            ...)
}

tbl.lsm <- function (x, only.significant=F, cols=c('contrast', 'estimate', 'p.value'),
                        estimate='%.3f', pval.args=list(fmt='%.3f', cutoff=0.001), justify='rcc', ...) {
    if ('lsm.list' %in% class(x) || 'emm_list' %in% class(x))
        x <- x[grepl('pairwise differences', names(x))][[1]]
    colnames=c(
        contrast='Comparison',
        estimate='Estimate',
        t.ratio='T ratio',
        p.value='$p$',
        SE='SE',
        df='df')
    cols = colnames[cols]
    if (any(is.na(cols)))
        stop("Some column names not recognised. Possiblities are ",
             paste(sprintf('%s', names(colnames)), collapse=', '))
    x <- data.table(summary(x))
    setnames(x, names(colnames), colnames)
    x[, Estimate:=sprintf(estimate, Estimate)]
    if (only.significant)
       x = x[`$p$` < 0.05]
    x[, `$p$`:=do.call(pval, c(list(`$p$`), pval.args))]
    tbl(x[, cols, with=F], justify=justify, ...)
    invisible(x)
}

# ----- graphs ----- #
# +theme_bw() or theme_minimal() or theme_classic(). library(Ggthemes)
# http://www.noamross.net/blog/2013/11/20/formatting-plots-for-pubs.html
# pdfs 600DPI?
ggplot2::theme_set(ggplot2::theme_bw()) # ?
rotx90 <- function () { ggplot2::theme(axis.text.x = ggplot2::element_text(angle = 90, hjust = 1, vjust=0.5)) }
zeroline <- function (lty='dotted', col='black', ...) { ggplot2::geom_hline(yintercept=0, lty=lty, col=col, ...) }
# CONSISTENT COLOUR SCALES FOR METHODS
# https://stackoverflow.com/questions/8197559/emulate-ggplot2-default-color-palette
gg_color_hue <- function(n) {
  hues = seq(15, 375, length = n + 1)
  hcl(h = hues, l = 65, c = 100)[1:n]
}

# ----- commonly-used graphics ---- #
# @TODO byrow, bycol, and pages
gen.diagnostic.image <- function (results, slice, groupvar, dataset, subdir, fbase='', dirbase='..', conservative=F, scale=1, progress=T, grouporder=NULL, groupsinrows=T, nrow=NULL, ncol=NULL, rot=F) {
    odir <- sprintf('%s/figure/%s/%s/%s/', dirbase, subdir, dataset, fbase)
    ofile <- sprintf('%s/figure/%s/%s_%sresults.png', dirbase, subdir, dataset, ifelse(fbase=='', '', paste0(fbase, '_')))
    if (conservative && file.exists(ofile)) {
        message(sprintf("File '%s' already exists", ofile))
        return()
    }
    if (!dir.exists(odir)) dir.create(odir)
    subjects <- unique(get(dataset)$subject)
    nsubj <- length(subjects)
    groupvals <- unique(results[[groupvar]])
    ngroup <- length(groupvals)
    if (!is.null(grouporder))
        groupvals <- c(intersect(grouporder, groupvals), setdiff(groupvals,grouporder))

    if (progress)
        pb <- txtProgressBar(min=0, max=nsubj + nrow(results) + 1, style=3)

    gts <- lapply(subjects, function (subj) {
        oofile <- file.path(dirname(odir), sprintf('%s_GT_slice%i.png', subj, slice))
        gt <- NULL
        if (!conservative || !file.exists(oofile) || any(!file.exists(file.path(odir, sprintf('%s_%s_slice%i.png', subj, groupvals, slice))))) {
            gt <- get.MRI(subset(get(dataset), type=='GT' & subject == subj), dat.dir)
            rr <- MRIres(gt)
            #imagemat(gt[,,slice], asp=rr[1]/rr[2])
            write.seg(gt[,,slice], oofile, mask=(gt[,,slice]>0), clip=T, asp=rr[1]/rr[2], max=3, scale=scale, rot=rot)
        #} else {
        #    message(sprintf("'%s' already exists", oofile))
        }
        if (progress) setTxtProgressBar(pb, getTxtProgressBar(pb) + 1)
        gt
    })
    names(gts) <- subjects
    for (i in 1:nrow(results)) { # what slice??
        oofile <- file.path(odir, sprintf('%s_%s_slice%i.png', results$subject[i], results[[groupvar]][i], slice))
        if (!conservative || !file.exists(oofile)) {
            gt <- gts[[results$subject[i]]]
            o <- load.seg(results[i], mask=gt > 0)
            rr <- MRIres(gt)
            write.seg(o[,,slice], oofile,
                      mask=(gt[,,slice]>0), clip=T, asp=rr[1]/rr[2], max=3, scale=scale, rot=rot)
        #} else {
        #    message(sprintf("'%s' already exists", oofile))
        }
        if (progress) setTxtProgressBar(pb, getTxtProgressBar(pb) + 1)
    }
    # montage {files} -tile {ncolxnrow} out.png
    # `y`/`ysmth` also?
    .tmp <- nrow
    if (!is.null(nrow) && !groupsinrows) { # switch
        nrow <- ncol
        ncol <- .tmp
    }
    nrow <- ifelse(is.null(nrow), ngroup + 1, nrow)
    ncol <- ifelse(is.null(ncol), nsubj, ncol)
    if (groupsinrows) {
        odirs <- rep(c(dirname(odir), odir), c(nsubj, nsubj*ngroup))
        cmd <- sprintf('montage %s -tile %ix%i %s',
                       paste(sprintf('-label %s %s', shQuote(outer(subjects, c('GT', groupvals), paste)),
                       shQuote(file.path(odirs, outer(subjects, c('GT', groupvals), sprintf, fmt='%s_%s_slice%i.png', slice)))), collapse=' '),
                       ncol, nrow, ofile)
    } else {
        odirs <- rep(c(dirname(odir), rep(odir, ngroup)), nsubj)
        cmd <- sprintf('montage %s -tile %ix%i %s',
                       paste(sprintf('-label %s %s', shQuote(t(outer(subjects, c('GT', groupvals), paste))),
                       shQuote(file.path(odirs, t(outer(subjects, c('GT', groupvals), sprintf, fmt='%s_%s_slice%i.png', slice))))), collapse=' '),
                       nrow, ncol, ofile)
    }
    #message(cmd)
    system(cmd)
    if (progress) setTxtProgressBar(pb, getTxtProgressBar(pb) + 1)
    if (progress) close(pb)
    invisible(ofile)
}

# calculate avg uijs
get.uijs <- function (r, mask, precomp=NULL) {
    if (is.null(precomp)) {
        precomp <- icx.setup(dim(mask), r$connectivity, mask=mask)
    }
    ww <- neighbourhood.weights(r$connectivity, MRIres(mask))
    o <- load.experiment(r, mask=mask)
    zi <- which.rowmax(o$o$prob)
    calc.us(switch(r$MRF.approx, MF=o$o$prob, PL=as.zij(zi, 3)), precomp=precomp, weights=ww)[cbind(seq_along(zi), zi)]
}

# Note: this changes the thing you pass in by reference.
calc.mean.uijs <- function (todo, save=FALSE) {
    pb <- txtProgressBar(min=0, max=nrow(todo), style=3)
    todo[, avg.uij:=NA_real_]
    for (subj in unique(todo$subject)) {
        m <- get.MRI(subset(get(todo[subject == subj, dataset[1]]), subject == subj & type == 'T1'), dat.dir) > 0
        for (cc in unique(todo$connectivity)) {
            pr <- icx.setup(dim(m), cc, mask=m)
            for (appr in unique(todo$MRF.approx)) {
                for (i in 1:nrow(todo[MRF.approx == appr & subject == subj & connectivity == cc])) {
                    r <- todo[MRF.approx == appr & subject == subj & connectivity == cc][i]
                    # avg ui[zi]
                    todo[md5sum == r$md5sum, avg.uij:=mean(get.uijs(r, m, pr))]
                    setTxtProgressBar(pb, getTxtProgressBar(pb) + 1)
                }
            }
        }
    }
    if (is.character(save)) {
        if (!file.exists(save))
            warning(sprintf("Not merging/saving results; file '%s' does not exist. Return updated input dt instead.", save))
        # merge into rfull
        rfull <- data.table(readRDS(save))
        setkey(rfull, md5sum)
        setkey(todo, md5sum)
        rfull[todo, avg.uij:=i.avg.uij]
        saveRDS(rfull, save)
    }
    todo
}

# df = rows of results with $slice and $row and $col [for circles] and $ofile
# if you want a GT, then ... feed in the appropriate row of IBS_Rnifti_stripped
# Note - doesn't really handle T1 images yet (GLMAX)
# Todo: odir == ../ch{chapter} or ../IBSR_nifti_stripped/?
# coordinates should be on the clipped image unless masked.coords=F then can be on the full image
saveImages <- function(df, odir, conservative=T, dat.dir='~/phd/data', masked.coords=T, quiet=!interactive()) {
    df <- data.table(df)
    ofiles <- file.path(odir, df$ofile)
    # @TODO should do it by subject to load the one GT
    j <- 0
    if (conservative && (!is.null(ofiles) && !any(is.na(ofiles)) && all(file.exists(ofiles)))) {
        if (!quiet)
            message("All files exist, skipping.")
        return(invisible(ofiles))
    }
    for (subj in unique(df$subj)) {
        ss = df[subject == subj]
        DAT = get(ss$dataset[1])
        gt = get.MRI(subset(DAT, subject == subj & type == 'GT'), dat.dir)
        m = (gt > 0)
        ar <- MRIres(m)
        ar <- ar[1]/ar[2]

        for (i in 1:nrow(ss)) {
            r = ss[i]
            j <- j + 1
            #j <- df[md5sum == r$md5sum, which=T]

            sl <- r$slice
            if (is.null(sl) || is.na(sl) || length(sl) == 0) sl <- round(dim(m)[3]/2)

            ofile <- file.path(odir, r$ofile)
            if (length(ofile) == 0 || is.null(ofile) || is.na(ofile))
                ofile <- file.path(odir, sprintf('%s_%s_slice%i.png', subj, r$type, sl))
            ofiles[j] <- ofile

            if (conservative && file.exists(ofile)) {
                if (!quiet)
                    message(sprintf("File '%s' already exists, not creating.", ofile))
                next
            }


            if (!is.null(r$out.file)) {# it's from results
                z = load.seg(r, mask=m)
            } else { # it's from e.g. IBSR_nifti_stripped. Just save it
                z = get.MRI(r, dat.dir)
            }


            # SAVE IMAGE
            MAX <- max(z)
            if (r$type == 'GT' || !is.na(r$out.file)) MAX <- 3

            write.seg(z[,,sl], ofile, mask=m[,,sl], clip=T, asp=ar,
                      max=MAX, scale=2, rot='counterclockwise')

            # DRAW CIRCLES
            if (!is.null(r$row) && !is.na(r$row) && length(r$row) > 0) {
                offs <- c(0,0)
                if (!masked.coords) offs <- sapply(get.ROI(m), min)
                names(ofiles)[j] <- drawCircles(ofile, r$row[[1]], r$col[[1]],
                            offs=c(0,0), asp=ar, quiet=T, radius=9)
            }
            ofiles[j] <- ofile
        }
    }
    invisible(ofiles)
}
aspect <- function (img) {
    rr <- MRIres(img)
    rr[1]/rr[2]
}

