# .Rprofile -- commands to execute at the beginning of each R session
#
# You can use this file to load packages, set options, etc.
#
# NOTE: changes in this file won't be reflected until after you quit
# and start a new session
#
local({
    options(stringsAsFactors=FALSE)
    options(datatable.print.class=TRUE)
    options(max.print=100)
if (Sys.getenv("RSTUDIO") == "1") {
    # set my PATH because RStudio doesn't load my bashrc?!?!

    # texlive
    #texlive.paths <- list.dirs(file.path(list.files(c('/windows/texlive', '/usr/local/texlive'), recursive=F, pattern='20[0-9]{2}', full.names=TRUE), 'bin'), recursive=F)
    #Sys.setenv(PATH=paste(paste(texlive.paths, collapse=':'), Sys.getenv("PATH"), sep=":"))

    # rvev
    # get currently-used ruby
    # system('rvm-prompt', intern=T) # <-- this works from terminal but not RStudio...
    #   something about RStudio's `system()` not being a login shell?
    #current.ruby <- '2.1.2'
    #Sys.setenv(PATH=paste(path.expand(sprintf('~/.rvm/gems/ruby-%s/wrappers', current.ruby)), Sys.getenv('PATH'), sep=':'))

    # pandoc
    pandoc.bin <- paste(path.expand(c('~/.local/bin', '~/.cabal/bin')), collapse=":")
    Sys.setenv(PATH=paste(pandoc.bin, Sys.getenv("PATH"), sep=":"))

    #if (require(simplecitr, quietly=T)) {
    #    simplecitr::set.bibfile('~/phd/papers/library.bib')
    #    simplecitr::set.check.bib(F)
    #}
}
})

# on Linux I get some problems sometimes with libcurl (some computers have it, some don't)
options(download.file.method = "wget")

# help style should be text for console-style R
if (Sys.getenv("RSTUDIO") != "1")
    options(help_type='text')


# Lines added by the Vim-R-plugin command :RpluginConfig (2014-Jul-28 13:46):
# currently broken
if(F && interactive() && Sys.getenv("RSTUDIO") != "1") {
    if(nchar(Sys.getenv("DISPLAY")) > 1)
        options(editor = 'gvim -f -c "set ft=r"')
    else
        options(editor = 'vim -c "set ft=r"')
    library(colorout)
    if(Sys.getenv("TERM") != "linux" && Sys.getenv("TERM") != ""){
        # Choose the colors for R output among 256 options.
        # You should run show256Colors() and help(setOutputColors256) to
        # know how to change the colors according to your taste:
        setOutputColors256(verbose = FALSE)
    }
    library(setwidth)
    library(vimcom)
}

local({
    # NEED library(fortunes)
    patch.fortunes <- function (fortune.directory='/usr/share/games/fortunes', use.iconv=T) {
        if (suppressWarnings(!require(fortunes, quietly=T))) {
            return()
        }

        source.files = rbind(
         data.frame(source='fortunes-bofh-excuses',
                    files='bofh-excuses.dat',
                    stringsAsFactors=F),
         data.frame(source='fortunes-debian-hints',
                    files='debian-hints.dat',
                    stringsAsFactors=F),
         data.frame(source='fortunes-mario',
                    files=c('mario.anagramas.dat',
                            'mario.arteascii.dat', 'mario.computadores.dat',
                            'mario.gauchismos.dat', 'mario.geral.dat', 'mario.palindromos.dat',
                            'mario.piadas.dat'),
                    stringsAsFactors=F),
         data.frame(source='fortunes-min',
                    files=c('fortunes.dat', 'literature.dat', 'riddles.dat'),
                    stringsAsFactors=F),
         data.frame(source='fortunes-ubuntu-server', files='ubuntu-server-tips.dat', stringsAsFactors=F),
         data.frame(source='fortunes',
                    files=c('art.dat', 'ascii-art.dat', 'computers.dat', 'cookie.dat',
                            'debian.dat', 'definitions.dat', 'disclaimer.dat', 'drugs.dat',
                            'education.dat', 'ethnic.dat', 'food.dat', 'goedel.dat',
                            'humorists.dat', 'kids.dat', 'knghtbrd.dat', 'law.dat',
                            'linuxcookie.dat', 'linux.dat', 'love.dat', 'magic.dat',
                            'medicine.dat', 'men-women.dat', 'miscellaneous.dat',
                            'news.dat', 'paradoxum.dat', 'people.dat', 'perl.dat',
                            'pets.dat', 'platitudes.dat', 'politics.dat', 'science.dat',
                            'songs-poems.dat', 'sports.dat', 'startrek.dat', 'tao.dat',
                            'translate-me.dat', 'wisdom.dat', 'work.dat', 'zippy.dat'),
                    stringsAsFactors=F)
        )
        source.file.map <- source.files$source
        names(source.file.map) <- sub('\\.dat$', '', source.files$files)

        # ----- extract
        files = list.files(fortune.directory, recursive=F, pattern='^[^.]+$')

        if (!length(files)) return()

        # Fortunes separated by newline with '%'
        f <- do.call(rbind,
          c(lapply(files, function (f) {
               l <- readLines(file.path(fortune.directory, f), warn=F)
               seps <- unique(c(1, which(l == '%'), length(l)))
               l[seps] <- ''
               l <- unlist(tapply(l, cut(seq_along(l), seps), paste, collapse='\n', simplify=F), use.names=F) # exclude last el of '%'
               # bylines
               # \n\t\t-- Author
               # (star trek) \n {spaces} [person[, year]]
               # hrm be smarter and look @ last line?
               # blah, some go over multiple lines! join & split back up?
               #gsub('\n( +)\\[(?=.+\\]$|(?=[^\\]\n]+\n\\1[^ ].*
               if (use.iconv) l <- iconv(l, to='ASCII', sub='')
               l <- strsplit(l, '\n\t\t-- ' )
               l <- data.frame(quote=gsub('\n$|^\n', '', vapply(l, '[', i=1, 'template')),
                          author=sub('\n$', '', vapply(l, '[', i=2, 'template')),
                          context=NA,
                          source=sprintf('%s.dat (%s)', f, source.file.map[f]),
                          date=NA,
                          stringsAsFactors=F)
               # Note: author is **required**
               l$author[is.na(l$author)] <- sprintf('Unknown (%s)', file.path(fortune.directory, f))

               # write out the fortunes (note - embedded newlines problem, make sure
               # files can be read in by
               # `read.table(file, header=T, sep=';', quote='"', colClasses='character')`
               # ofile <- sprintf('~/r-fortunes/fortunes/%s.csv', f)
               # write.table(l, file=ofile, sep=';', row.names=F, quote=T)
               l
          })
        , make.row.names=F, stringsAsFactors=F))

        # try extract dates?...nah, can't BB

        # ------ patch
        # environments are by reference...
        x = fortunes:::fortunes.env
        if (is.null(x$fortunes.data))
            x$fortunes.data <- fortunes::read.fortunes()

        if (!exists('.PATCHED_FORTUNES', envir=x, mode='logical') || !x$.PATCHED_FORTUNES) {
            x$.PATCHED_FORTUNES <- T
            x$fortunes.data <- rbind(x$fortunes.data, f)
        }
    }

    # @TODO: do it on attach (setHook)
    if (interactive() && suppressWarnings(require(fortunes, quietly=T))) {
        patch.fortunes()
        if (suppressWarnings(require(cowsay, quietly=T))) {
            cowsay::cowsay(fortunes::fortune())
        } else {
            fortunes::fortune()
        }
    }
})

options(max.print=300)
rotx90 <- function () { ggplot2::theme(axis.text.x = ggplot2::element_text(angle = 90, hjust = 1, vjust=0.5)) }
zeroline <- function (lty='dotted', col='black', ...) { ggplot2::geom_hline(yintercept=0, lty=lty, col=col, ...) }
