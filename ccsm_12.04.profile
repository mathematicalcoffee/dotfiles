[ring]
s0_min_scale = 0.800000

[gnomecompat]
s0_main_menu_key = Disabled
s0_run_key = Disabled

[core]
s0_active_plugins = core;composite;opengl;decor;snap;resize;imgpng;resizeinfo;move;grid;compiztoolbox;unitymtgrabhandles;regex;mousepoll;place;session;animation;gnomecompat;workarounds;expo;wall;ezoom;staticswitcher;fade;scale;unityshell;

[expo]
s0_expo_key = F8
s0_expo_edge = BottomRight

[winrules]
s0_size_matches = (name=gnome-terminal) & title=NAO;
s0_size_width_values = 482;
s0_size_height_values = 314;

[resizeinfo]
s0_always_show = true

[staticswitcher]
s0_highlight_mode = 1

[grid]
s0_put_top_key = <Control><Primary><Super>KP_8
s0_put_bottom_key = <Control><Primary><Super>KP_2
s0_put_topleft_key = <Control><Primary><Super>KP_7
s0_put_topright_key = <Control><Primary><Super>KP_9
s0_put_bottomleft_key = <Control><Primary><Super>KP_1
s0_put_bottomright_key = <Control><Primary><Super>KP_3
s0_put_restore_key = <Control><Primary><Super>KP_5

[unityshell]
s0_launcher_switcher_forward = Disabled
s0_launcher_switcher_prev = Disabled
s0_alt_tab_forward = Disabled
s0_alt_tab_prev = Disabled
s0_launcher_capture_mouse = false

[scale]
s0_initiate_all_edge = BottomLeft
s0_initiate_all_key = F9

