### bash aliases
alias dum='du -h --max-depth'

alias duh='du -h --summarize'
alias lslarge='du -h --summarize * | grep -P '"'"'^([0-9.]+G|[0-9]{2,}(.[0-9]+)?M)'"'"
alias random='head /dev/urandom | uuencode -m - | sed -n 2p | cut -c1-${1:-8}'

## there is another program ack which is not the same as ack-grep, but I won't
##  use it ever (it's a Japanese character checker/converter)
if [ -f /usr/bin/ack-grep ]; then
    alias ack='ack-grep'
fi

# these ones seemto be default (type 'alias')
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias l.='ls -d .* --color=auto'
alias ls='ls --color=auto'

# awesome. Use for long-running commands like so: sleep 1; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias solve='Rscript -e'

# the following depends on which `which` you have...(varies a lot between flavours)
# alias which='alias | /usr/bin/which --tty-only --read-alias --show-dot --show-tilde'

# for dual-boot machines
# alias matlab='/usr/local/matlab/bin/matlab'
# alias mush='wine /windows/Program\ Files/mushclient/MUSHclient.exe /wine &' # note: do i want to install in wine instead
# alias utorrent='wine /windows/Program\ Files/uTorrent/uTorrent.exe'
# alias 7zultra="7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on"
# alias compresspdf="gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=out.pdf -f" #input.pdf <-- make a script instead
#  can also try -dPDFSETTINGS=/screen or /ebook or /printer (72/150/300dpi)


#alias vim='vim --servername `random`'
#TODO: should grep for +x (or w/e the feature is) in vim --version
if [ "`which vim`" == "/usr/local/bin/vim" ]; then
    alias vim='vim --servername VIM'
fi

alias mygjs='gjs --include-path="/usr/lib/girepository-1.0"'
# BAH: have to make symlinks for the rest. /usr/lib/mutter, /usr/lib/gnome-shell

# copy to clipboard
alias clip='xclip -selection c'

# sudo preserving path
alias psudo='sudo env PATH="$PATH"'

# if todo.txt CLI is installed.
if [ -d $HOME/.todo ]; then
    alias t='todo.sh'
fi

# black python code linter from pip3 install black
alias black='python3 ~/.local/lib/python3.6/site-packages/black.py'

alias pdiff='pycharm-community diff'
